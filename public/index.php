<?php
header('Content-Type:text/html;charset=UTF-8');
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $messages=array();
    $errors = array();
    $values = array();
    if (!empty($_COOKIE['save'])) {
        setcookie('save','',100000);
        setcookie('login', '',100000);
        setcookie('pass', '', 100000);
        $messages['save'] = 'Спасибо, результаты сохранены!';
        if (!empty($_COOKIE['pass']) && empty($_SESSION['login'])) {
            $messages['login_and_password'] = sprintf('Вы можете <a href="login.php">осуществить вход</a> по логину <strong>%s</strong>
            и паролю <strong>%s</strong> для изменения данных.',
            strip_tags($_COOKIE['login']),
            strip_tags($_COOKIE['pass']));
          }
    }
    
    $flag=FALSE;
    $super_separated='';
    $errors['name'] = !empty($_COOKIE['name_error']);
    $errors['mail'] = !empty($_COOKIE['mail_error']);
    $errors['year'] = !empty($_COOKIE['year_error']);
    $errors['sex'] = !empty($_COOKIE['sex_error']);
    $errors['limbs'] = !empty($_COOKIE['limbs_error']);
    $errors['super'] = !empty($_COOKIE['super_error']);
    $errors['check1'] = !empty($_COOKIE['check1_error']);
    $errors['biography'] = !empty($_COOKIE['biography_error']);
    if ($errors['name']) {
        if($_COOKIE['name_error']=='none'){
            setcookie('name_error','',100000);
            $messages['name'] = '<div class="error">Имя не должно быть пустым!</div>';
    }
    if($_COOKIE['name_error']=='Unacceptable symbols'){
            setcookie('name_error','',100000);
            $messages['name'] = '<div class="error">Недопустимые символы в имени:а-я,А-Я,0-9,\ - _ </div>';
        }
    }
   if ($errors['mail']) {
        if($_COOKIE['mail_error']=='none'){
            setcookie('mail_error','',100000);
            $messages['mail'] = '<div class="error">Укажите почту!</div>';
        }
        if($_COOKIE['mail_error']=='invalid address'){
            setcookie('mail_error','',100000);
            $messages['mail'] = '<div class="error">Почта указана некорректно.Пример:mail777@gmail.com</div>';
        }
    }
    if($errors['year']){
        setcookie('year_error','',100000);
        $messages['year'] = '<div class="error">Год рождения не заполнен!</div>';
    }
    if($errors['sex']){
            setcookie('sex_error','',100000);
            $messages['sex'] = '<div class="error">Укажите пол!</div>';
    }
    if ($errors['limbs']) {
        setcookie('limbs_error','',100000);
        $messages['limbs'] = '<div class="error">Укажите количество конечностей!</div>';
    }
       if($errors['super']){
        if($_COOKIE['super_error']=="none"){
            setcookie('super_error','',100000);
            $messages['super'] = '<div class="error">Способности должны быть выбраны!</div>';
        }
        if($_COOKIE['super_error']=="noneselected"){
            setcookie('super_error','',100000);
            $messages['super'] = '<div class="error">Что-то не сходится... Уберите "None" и попробуйте еще раз!</div>';
        }
    }
    if ($errors['biography']) {
            setcookie('biography_error','',100000);
            $messages['biography'] = '<div class="error">Черканите на память!</div>';

    }
  if($errors['check1']){
        setcookie('check1_error','',100000);
        $messages['check1'] = '<div class="error">Вы не согласились с контрактом</div>';
    }
    $values['name'] = empty($_COOKIE['name_value']) ? '' : strip_tags($_COOKIE['name_value']);
    $values['mail'] = empty($_COOKIE['mail_value']) ? '' : strip_tags($_COOKIE['mail_value']);
    $values['year'] = empty($_COOKIE['year_value']) ? '' : strip_tags($_COOKIE['year_value']);
    $values['sex'] = empty($_COOKIE['sex_value']) ? '' : strip_tags($_COOKIE['sex_value']);
    $values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : strip_tags($_COOKIE['limbs_value']);
    $values['super'] = empty($_COOKIE['super_value']) ? '' : strip_tags($_COOKIE['super_value']);
    $values['biography'] = empty($_COOKIE['biography_value']) ? '' : strip_tags($_COOKIE['biography_value']);
    $values['check1'] = empty($_COOKIE['check1_value']) ? '' : strip_tags($_COOKIE['check1_value']);
    if (session_start() && !empty($_COOKIE[session_name()]) && !empty($_SESSION['login'])) {
        $user = 'u20304';
        $password = '6681182';
        $log=$_SESSION['login'];
        $db = new PDO('mysql:host=localhost;dbname=u20304', $user, $password,
        array(PDO::ATTR_PERSISTENT => true));
        try{
        $stmt = $db->prepare("SELECT name,mail,birth,sex,limbs,super,bio,check1 FROM info WHERE login = '$log'");
        $stmt->execute();
            while ($row = $stmt->fetch(PDO::FETCH_LAZY))
            {
                 $values['name']=$row['name'];
                 $values['mail']=$row['mail'];
                 $values['year']=$row['birth'];
                 $values['sex']=$row['sex'];
                 $values['limbs']=$row['limbs'];
                 $values_f=array();
                 if(!empty($row['super'])){
                    if(stristr($row['super'],'net') == TRUE) {
                      array_push($values_f,'net');
                    }
                    if(stristr($row['super'],'godmod') == TRUE) {
                      array_push($values_f,'godmod');
                    }
                    if(stristr($row['super'],'levitation') == TRUE) {
                      array_push($values_f,'levitation');
                    }
                    if(stristr($row['super'],'unvisibility') == TRUE) {
                      array_push($values_f,'unvisibility');
                    }
                    if(stristr($row['super'],'telekinesis') == TRUE) {
                      array_push($values_f,'telekinesis');
                    }
                    if(stristr($row['super'],'extrasensory') == TRUE) {
                      array_push($values_f,'extrasensory');
                    }
                    $values['super']=serialize($values_f);
                  }
                 $values['biography']=$row['bio'];
                 $values['check1']=$row['check1'];
            }
    }catch(PDOException $e){
        print('Error : ' . $e->getMessage());
        exit();
        } 
    }
    include('form.php');
}
else {
    /*Проверяем на ошибки*/
    $errors = FALSE;
            if (empty($_POST['name'])) {
                setcookie('name_error', 'none', time() + 24 * 60 * 60);
                setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60);
                $errors = TRUE;
            }
            else {
                if (!preg_match("#^[aA-zZ0-9\-_]+$#",$_POST['name'])){
                    setcookie('name_error', 'Unacceptable symbols', time() + 24 * 60 * 60);
                    setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60);
                    $errors=TRUE;
                }else{
                    setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60);
                }
            }
          if (empty($_POST['mail'])) {
                setcookie('mail_error', 'none', time() + 24 * 60 * 60);
                setcookie('mail_value', $_POST['mail'], time() + 30 * 24 * 60 * 60);
                $errors = TRUE;
            }
            else{
                if (!preg_match("/^(?:[a-z0-9]+(?:[-_.]?[a-z0-9]+)?@[a-z0-9_.-]+(?:\.?[a-z0-9]+)?\.[a-z]{2,10})$/i", $_POST['mail'])) {
                    setcookie('mail_error', 'invalid address', time() + 24 * 60 * 60);
                    setcookie('mail_value', $_POST['mail'], time() + 30 * 24 * 60 * 60);
                    $errors = TRUE;
                }else{
                    setcookie('mail_value', $_POST['mail'], time() + 30 * 24 * 60 * 60);
                }
            }
            if (empty($_POST['year'])) {
                setcookie('year_error', 'none', time() + 24 * 60 * 60);
                setcookie('year_value', $_POST['year'], time() + 30 * 24 * 60 * 60);
                $errors = TRUE;
            }
            else{
                setcookie('year_value', $_POST['year'], time() + 30 * 24 * 60 * 60);
            }
            if (empty($_POST['sex'])) {
                setcookie('sex_error', 'none', time() + 24 * 60 * 60);
                setcookie('sex_value', $_POST['sex'], time() + 30 * 24 * 60 * 60);
                $errors = TRUE;
            }
            else{
                setcookie('sex_value', $_POST['sex'], time() + 30 * 24 * 60 * 60);
            }
            if (empty($_POST['limbs'])) {
                setcookie('limbs_error', 'none', time() + 24 * 60 * 60);
                setcookie('limbs_value', $_POST['limbs'], time() + 30 * 24 * 60 * 60);
                $errors = TRUE;
            }
            else{
                setcookie('limbs_value', $_POST['limbs'], time() + 30 * 24 * 60 * 60);
            }
            if(!isset($_POST['super'])){
                setcookie('super_error', 'none', time() + 24 * 60 * 60);
                setcookie('super_value', serialize($_POST['super']), time() + 30 * 24 * 60 * 60);
                $errors = TRUE;
            }
            else{
                $super_mass=$_POST['super'];
                $flag=FALSE;
                for($w=0;$w<count($super_mass);$w++){
                    if($super_mass[$w]=="net"){
                        $flag=TRUE;break;
                    }
                }
                if($flag && count($super_mass)!=1){
                    setcookie('super_error', 'noneselected', time() + 24 * 60 * 60);
                    setcookie('super_value',serialize($_POST['super']), time() + 30 * 24 * 60 * 60);
                    $errors = TRUE;
                }else{
                    setcookie('super_value',serialize($_POST['super']), time() + 30 * 24 * 60 * 60);
                }
            }
			if (empty($_POST['biography'])) {
                setcookie('biography_error', 'none', time() + 24 * 60 * 60);
                setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
                $errors = TRUE;
            }
            else{
                setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
             }
            if (empty($_POST['check1'])) {
                setcookie('check1_error', 'none', time() + 24 * 60 * 60);
                setcookie('check1_value', $_POST['check1'], time() + 30 * 24 * 60 * 60);
                $errors = TRUE;
            }
            else{
                setcookie('check1_value', $_POST['check1'], time() + 30 * 24 * 60 * 60);
            }
            if ($errors) {
                header('Location:index.php');
                exit();
            }
            else {/*удаляем куки с ошибками*/
                setcookie('name_error', '', 100000);setcookie('limbs_error', '', 100000);
                setcookie('mail_error', '', 100000);setcookie('super_error', '', 100000);
                setcookie('year_error', '', 100000);setcookie('biography_error', '', 100000);
                setcookie('sex_error', '', 100000);setcookie('check1_error', '', 100000);
            }

       if (session_start() && !empty($_COOKIE[session_name()]) && !empty($_SESSION['login'])  && !empty($_COOKIE['login'])) 
        {//перезапись по логину
        $user = 'u20304';
        $password = '6681182';
        $super_separated='';
        $log=$_SESSION['login'];
        $db = new PDO('mysql:host=localhost;dbname=u20304', $user, $password,array(PDO::ATTR_PERSISTENT => true));
        try {
        $stmt = $db->prepare("UPDATE info SET name=?,mail=?,birth=?,sex=?,limbs=?,super=?,bio=?,check1=? WHERE login='$log' ");
        
        $name=$_POST["name"];
        $mail=$_POST["mail"];
        $birth=$_POST["year"];
        $sex=$_POST["sex"];
        $limbs=$_POST["limbs"];
        if(!empty($_POST['super'])){
            $super_mass=$_POST['super'];
            for($w=0;$w<count($super_mass);$w++){
                if($flag){
                    if($super_mass[$w]!="net")unset($super_mass[$w]);
                    $super_separated=implode(' ',$super_mass);
                }else{
                    $super_separated=implode(' ',$super_mass);
                }
            }
        }
        $super=$super_separated;
        $bio=$_POST["biography"];
        $check1=$_POST["check1"];
        
        $stmt->execute(array($name,$mail,$birth,$sex,$limbs,$super,$bio,$check1,));
        }catch(PDOException $e){
            print('Error : ' . $e->getMessage());
            exit();
        }
    }else 
    {   
        $logins_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
        $pass_chars='0123456789abcdefghijklmnopqrstuvwxyz-_';
        $login = substr(str_shuffle($logins_chars), 0, 3);
        $password =substr(str_shuffle($pass_chars),0,3);
        // Сохраняем в Cookies.
        setcookie('login', $login);
        setcookie('pass', $password);
        /*запись в бд*/
        if(!empty($_POST['super'])){
            $super_mass=$_POST['super'];
            for($w=0;$w<count($super_mass);$w++){
                if($flag){
                    if($super_mass[$w]!="net")unset($super_mass[$w]);
                    $super_separated=implode(' ',$super_mass);
                }else{
                    $super_separated=implode(' ',$super_mass);
                }
            }
        }
        $user = 'u20304';
        $pass = '6681182';
        $db = new PDO('mysql:host=localhost;dbname=u20304', $user, $pass,
        array(PDO::ATTR_PERSISTENT => true));
        try {
        $stmt = $db->prepare("INSERT INTO info(name,login,password,mail, birth, sex, limbs,super,bio,check1) 
        VALUES (:name,:login,:password,:mail, :birth, :sex, :limbs,:super,:bio, :check1)");
        $stmt->bindParam(':name', $name_db);
        $stmt->bindParam(':login', $login_db);
        $stmt->bindParam(':password', $pass_db);
        $stmt->bindParam(':mail', $mail_db);
        $stmt->bindParam(':birth', $year_db);
        $stmt->bindParam(':sex', $sex_db);
        $stmt->bindParam(':limbs', $limb_db);
        $stmt->bindParam(':super', $super_db);
        $stmt->bindParam(':bio', $bio_db);
        $stmt->bindParam(':check1', $check1_db);
        $name_db=htmlspecialchars($_POST["name"]);
        $login_db=$login;
        $pass_db=md5($password);
        $mail_db=htmlspecialchars($_POST["mail"]);
        $year_db=$_POST["year"];
        $sex_db=$_POST["sex"];
        $limb_db=$_POST["limbs"];
        $super_db=$super_separated;
        $bio_db=htmlspecialchars($_POST["biography"]);
        $check1_db=$_POST["check1"];
        $stmt->execute();

        }
        catch(PDOException $e){
        print('Error : ' . $e->getMessage());
        exit();
            }
        }
    setcookie('save', '1');
    header('Location:index.php');
}
?>
